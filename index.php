<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>
        </title>
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.css" />
        <link rel="stylesheet" href="css/my.css" />
        <style>
            /* App custom styles */
            .contentWrapper{padding: 0 25px;}
        </style>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
        </script>
        <script src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js">
        </script>
        <script src="http://modernizr.com/downloads/modernizr-2.5.3.js">
        </script>
        <script src="js/my.js">
        </script>
    </head>
    <body>
        <!-- Home -->
        <div data-role="page" id="page1">
            <header data-theme="a" data-role="header">
                <h1>
                    alAugustin();
                </h1>
            </header>
            <div data-role="navbar" data-iconpos="left">
				
				<jdoc:include type="modules" name="topNav" />
			</div>
            <div data-role="content" style="padding: 15px">
				
				<div id="mainComponent" class="contentWrapper">
					<jdoc:include type="component" />
				</div>
				<div id="tweets" class="contentWrapper">
					<jdoc:include type="modules" name="footTweets" />
				</div>            	
            	<div data-role="controlgroup">
					<a href="http://twitter.com/augustinaa" target="_blank" data-transition="pop" data-role="button">Twitter</a>
					<a href="http://www.facebook.com/al.augustin" target="_blank" data-transition="pop" data-role="button">Facebook</a>
					<a href="http://alaugustin.tumblr.com/" target="_blank" data-transition="pop" data-role="button">Tumblr</a>
					<a href="http://ca.linkedin.com/in/alaugustin" target="_blank" data-transition="pop" data-role="button">LinkedIn</a>
				</div>            	
            </div>
            <footer data-theme="a" data-role="footer" data-position="fixed">                
                <jdoc:include type="modules" name="breadcrumb" />
                <jdoc:include type="modules" name="footer" />
            </footer>
        </div>
        
        <script>
            //App custom javascript
        </script>
    </body>
</html>
